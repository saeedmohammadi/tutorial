	/**
	 * Adds a watcher to all of the supplied issues. If there is partial
	 * success, the issues which we can modify will be modified and the ones we
	 * cannot will be returned in an ArrayList.
	 * 
	 * @param issues
	 *            the list of issues to update
	 * @param currentUser
	 *            the user to run the operation as
	 * @param watcher
	 *            the watcher to add to the issues
	 * @return an ArrayList<Issue> containing the issues that could not be
	 *         modified
	 */
	public ArrayList<Issue> addWatcherToAll(final ArrayList<Issue> issues,
			final User currentUser, final User watcher) {
		ArrayList<Issue> successfulIssues = new ArrayList<Issue>();
		ArrayList<Issue> failedIssues = new ArrayList<Issue>();
		for (Issue issue : issues) {
			if (canWatchIssue(issue, currentUser, watcher)) {
				successfulIssues.add(issue);
			} else {
				failedIssues.add(issue);
			}
		}
		/*
		 * SM: Why should watching start for an empty list? 
		 */
		if (!successfulIssues.isEmpty()) {
			watcherManager.startWatching(currentUser, successfulIssues);
		}
		return failedIssues;
	}

	private boolean canWatchIssue(Issue issue, User currentUser, User watcher) {
		/*
		 * SM: the logical operator should be AND while in the code it is OR
		 */
		if (currentUser.equals(watcher)
				&& currentUser.getHasPermissionToModifyWatchers()) {
			return issue.getWatchingAllowed(watcher);
		}
		/*
		 * SM: instead of true, default return value must be false to avoid 
		 * permission violation
		 */
		return false;
	}

